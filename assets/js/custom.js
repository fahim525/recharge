
var maxAppend = 0;
var i=0;
var numbers = [];
var count = [];

var inputError = $('#inputError');
var emialInputError = $('#emailInputError');
var submitButton = document.getElementById("submit");

$('#add').on('click',function (e) {
    e.preventDefault();
    var  number =  $('input[name="number[]"]').val();
    var amount =  $('input[name="amount[]"]').val();
    var type =  $('input[name="type[]"]:checked').val();
    numbers.push(number);
    if(maxAppend > 0){
        for( i=0; i<numbers.length; i++ ){
            if(number  === numbers[i-1]){
                var same = 25;
            }
        }
    }
/*
Enable more record inserted

*/
    if(count === 1){
        maxAppend = maxAppend - 1;
        count = 0;
    }else if(count === 3){
        maxAppend = maxAppend - 2;
        count = 0;
    }else if(count === 6){
        maxAppend = maxAppend - 3;
        count = 0;
    }else if(count === 10){
        maxAppend = maxAppend - 4;
        count = 0;
    }

    if (maxAppend < 4) {
        if(number.length > 1) {
            if(number.length === 11){
                if(amount.length !== 0){
                    if(amount > 9){
                        if(same !== 25){
                            $('#view').append('<tr>' +
                                '<td>'+number+'</td>'+
                                '<input type="hidden" name="number[]" value="'+number+'">'+
                                '<td>'+amount+'</td>'+
                                '<input type="hidden" name="amount[]" value="'+amount+'">'+
                                '<td>'+type+'</td>'+
                                '<input type="hidden" name="type[]" value="'+type+'">'+
                                '<td class="text-center removeItem">'+
                                '<span class=" fa fa-trash text-danger"></span>' +
                                '</td>'+
                                '</tr>');
                            maxAppend++;
                            $('#number').val('');
                            $('#amount').val('');
                        }else {
                            inputError.addClass('text-danger').text('This number already added');
                        }
                    }else {
                        inputError.addClass('text-danger').text('Amount can not be less then 10');
                    }
                }else {
                    inputError.addClass('text-danger').text('Amount can not be empty ');
                }
            }else {
                inputError.addClass('text-danger').text('Number must be 11 digit');
            }
        }else {
            inputError.addClass('text-danger').text('Number and Amount can not be empty ');
        }
    }else {
        inputError.addClass('text-danger').text('Only 4 numbers allowed one time');
    }

   // console.log(maxAppend);

    if(maxAppend === 4){
            document.getElementById('number').setAttribute("disabled", "disabled");
            document.getElementById('amount').setAttribute("disabled", "disabled");
            inputError.addClass('text-danger').text('Only 4 numbers allowed one time');
    }

    removeItem();

});

function removeItem () {
    $('.removeItem').bind('click',function () {
        $(this).parent().remove();
        var removedNumber = $(this).parent().children().html();
        numbers.splice($.inArray(removedNumber,numbers) ,1);
        count =  count*1 + 1;
        if(maxAppend === 4){
            document.getElementById('number').removeAttribute("disabled");
            document.getElementById('amount').removeAttribute("disabled");
            inputError.removeClass('text-danger').text('');
        }
    });
}

$('input').on('keydown', function () {
    inputError.removeClass('text-danger').text('');
    emialInputError.addClass('text-danger').text('');
    submitButton.removeAttribute("disabled");
});



function getNumber(e){
    var inputLength = e.value.length; // input number length
    var inputValue = e.value;         // input field value
    e.value = e.value.replace(/[^0-9]/g, '');
    if(inputLength > 2)
    {
        var operator = inputValue.slice(0,3);
        if(operator !== '017' && operator !== '018' && operator.slice(0,3) !== '019' && operator !== '015' && operator !== '016' && operator !== '013' )
        {
            inputError.addClass('text-danger').text('Invalid Operator Number '+inputValue);
            submitButton.setAttribute("disabled", "disabled");
        }else {
            submitButton.removeAttribute("disabled");
            inputError.removeClass('text-danger').text('');
        }
    }
}

$('#number').on('keyup', function (e) {
    e.preventDefault();
    var  number =  $('input[name="number[]"]').val();
    var errorInput = $('#inputError');
    if(maxAppend > 0){
        for( i=0; i<numbers.length; i++ ){
            if(number.length === 11){
                if(number  === numbers[i]){
                    errorInput.addClass('text-danger').text('This number already added');
                    submitButton.setAttribute("disabled", "disabled");
                }
            }
        }
    }
});

function getAmount(e) {
    $('.equipCatValidation').on('keyup', function(e){
        if ($(this).val() > 1000
            && e.keyCode != 46
            && e.keyCode != 8
        ) {
            e.preventDefault();
            $(this).val(1000);
            inputError.addClass('text-danger').text('Amount should be maximum 1000');
        }
    });

    e.value = e.value.replace(/[^0-9]/g, '');
   if(e.value<10){
       inputError.addClass('text-danger').text('Amount should be minimum 10');
   }

}


$(submitButton).on('click',function () {
    var email   =  $('input[type="email"]').val();
    var number  =  $('input[name="number[]"]').val();
    var amount  =  $('input[name="amount[]"]').val();
    if(count === 1){
        maxAppend = maxAppend - 1;
        count = 0;
    }
    if(email.length<1 && number.length<1 && amount.length<1 && maxAppend<1 ){
        inputError.addClass('text-danger').text('All fields required');
        submitButton.setAttribute("disabled", "disabled");
    }else {
        if(email.length<1){
            inputError.addClass('text-danger').text('Email field required');
            submitButton.setAttribute("disabled", "disabled");
        }else{
            if(maxAppend<1 && number.length<1){
                inputError.addClass('text-danger').text('Number can not be empty');
                submitButton.setAttribute("disabled", "disabled");
            }else if(maxAppend<1 && amount.length<1){
               inputError.addClass('text-danger').text('Amount can not be empty');
               submitButton.setAttribute("disabled", "disabled");
            }
        }
    }

});



function getEmail(e) {

    var email   = e.value;
    var atpos   = email.indexOf("@");
    var dotpos  = email.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        emialInputError.addClass('text-danger').text('E-mail address not valid');
        submitButton.setAttribute("disabled", "disabled");
    }else {
        emialInputError.removeClass('text-danger').text('');
        submitButton.removeAttribute("disabled");
    }

}

$(document).ready(function(){
    $('input').iCheck({
        checkboxClass: 'icheckbox_flat',
        radioClass: 'iradio_flat-blue'
    });
});

$('footer').wrap('<div class="place-holder"></div>');

$('.place-holder').height($('footer').outerHeight());