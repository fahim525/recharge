<?php
session_start();

if(!isset($_SESSION['admin']) && $_SERVER['REQUEST_METHOD'] != 'POST'){
    header('location:index.php');
}

include_once '../../Controller/Recharge.php';
$obj = new Recharge();
$all_data = $obj->all();
?>

<!DOCTYPE html>
<html>
<head>
	<title>view Data</title>
	<title>Recharge System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/style.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Number</th>
							<th>Amount</th>
							<th>Type</th>
							<th>Created Time</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($all_data as $data)
							{
								?>
                                    <tr>
                                        <td><?=$data['number']?></td>
                                        <td><?=$data['amount']?></td>
                                        <td><?=($data['type']=='0')? 'Prepaid':'Postpaid' ?></td>
                                        <td><?=date('F j, Y, g:i a',strtotime($data['created_at']))?></td>
                                        <td>

                                            <?php
                                             if($data['status'] == 3){
                                                    echo '<p class="badge badge-primary">Padding</p>';
                                                }elseif ($data['status'] == 4){
                                                 echo '<p class="badge badge-success">Success</p>';
                                                }else{
                                                 echo '<p class="badge badge-warning">Failed</p>';
                                                }
                                            ?>
                                        </td>
                                    </tr>
								<?php
							}
						?>
					</tbody>
				</table
			</div>
		</div>
	</div>
</body>
</html>