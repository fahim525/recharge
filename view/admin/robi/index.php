<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
use App\Operator;
$op = new Operator();
$all_data = $op->operator('18');

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Robi Number
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <?php
                            $data = $op->select("select count(number) as total_number from recharge WHERE operator = '18'");
                            echo $data['total_number'];
                            ?>
                        </h3>

                        <p>Total Number</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <?php
                            $data = $op->select("select count(number) as total_success from recharge WHERE status = '2' AND operator = '18'");
                            echo $data['total_success'];
                            ?>
                        </h3>
                        <p>Total Success</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">

                        <h3>
                            <?php
                            $data = $op->select("select count(number) as total_pending from recharge WHERE status = '1' AND operator = '18'");
                            echo $data['total_pending'];
                            ?>
                        </h3>

                        <p>Total Pending</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            <?php
                            $data = $op->select("select count(number) as total_fail from recharge WHERE status = '3' AND operator = '18'");
                            echo $data['total_fail'];
                            ?>
                        </h3>

                        <p>Total Fail</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Robi Phone Number Here</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Amount</th>
                                <th>Type</th>
                                <th>Created Time</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($all_data as $data)
                        {
                            ?>
                            <tr>
                                <td><?=$data['number']?></td>
                                <td><?=$data['amount']?></td>
                                <td><?=($data['type']=='1')? 'Prepaid':'Postpaid' ?></td>
                                <td><?=date('F j, Y, g:i a',strtotime($data['created_at']))?></td>
                                <td>

                                    <?php
                                    if($data['status'] == 1){
                                        echo '<p class="badge badge-primary">Pending</p>';
                                    }elseif ($data['status'] == 2){
                                        echo '<p class="badge badge-success">Success</p>';
                                    }else{
                                        echo '<p class="badge badge-warning">Failed</p>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
    include_once '../includes/footer.php';
?>