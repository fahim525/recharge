<?php
include_once '../includes/header.php';

//$db = new PDO('mysql:host=localhost;dbname=uttaraex_topup', 'uttaraex_topup', 'topup18546975sdTyd*^7%$@!d');
$db = new PDO('mysql:host=localhost;dbname=test', 'root', '');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$stm = $db->prepare('SELECT * FROM `inbox`');
$stm->execute();
$all_data = $stm->fetchAll(PDO::FETCH_ASSOC);


?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inbox Data হোম
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Main row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Inbox information shows here</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sender</th>
                                        <th>Body</th>
                                        <th>SMS Date</th>
                                        <th>SMSC</th>
                                        <th>Operator</th>
                                        <th>Trx ID</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Paid</th>
                                        <th>Paid Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($all_data as $data)
                                    {
                                        ?>
                                        <tr>
                                            <td><?=$i++?></td>
                                            <td><?=$data['sender']?></td>
                                            <td><?=$data['body']?></td>
                                            <td><?=$data['smsdate']?></td>
                                            <td><?=$data['smsc']?></td>
                                            <td><?=$data['operator']?></td>
                                            <td><?=$data['trxid']?></td>
                                            <td><?=$data['date']?></td>
                                            <td><?=$data['time']?></td>
                                            <td><?=$data['paid']?></td>
                                            <td><?=$data['paiddate']?></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>

                                </table>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <!-- /.row (main row) -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php
include_once '../includes/footer.php';
?>