<?php
session_start();
if(($_SESSION['admin'] != '2#$@GDFE#@')){
    header('location:../../admin/index.php');
}
include_once 'includes/header.php';
include_once '../../vendor/autoload.php';
use App\Recharge;
$obj = new Recharge();
$op = new \App\Operator();
$all_data = $obj->all();
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                  <?php
                 $data = $op->select("select count(number) as total_number from recharge");
                 echo $data['total_number'];
                  ?>
              </h3>

              <p>Total Number</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                  <?php
                  $data = $op->select("select count(number) as total_success from recharge WHERE status = '4'");
                  echo $data['total_success'];
                  ?>
              </h3>
              <p>Total Success</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">

                <h3>
                    <?php
                    $data = $op->select("select count(number) as total_pending from recharge WHERE status = '3'");
                    echo $data['total_pending'];
                    ?>
                </h3>

              <p>Total Pending</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    <?php
                    $data = $op->select("select count(number) as total_fail from recharge WHERE status = '5'");
                    echo $data['total_fail'];
                    ?>
                </h3>

              <p>Total Fail</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Grameen Phone</span>
                        <span class="info-box-number">
                            <?php
                            $data = $op->select("select count(number) as total_number from recharge WHERE operator = '017'");
                            echo  $data['total_number'];
                            ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Banglalink</span>
                        <span class="info-box-number">
                          <?php
                          $data = $op->select("select count(number) as total_number from recharge WHERE operator = '019'");
                          echo $data['total_number'];
                          ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Airtel</span>
                        <span class="info-box-number">
                          <?php
                          $data = $op->select("select count(number) as total_number from recharge WHERE operator = '016'");
                          echo $data['total_number'];
                          ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Robi</span>
                        <span class="info-box-number">
                          <?php
                          $data = $op->select("select count(number) as total_number from recharge WHERE operator = '018'");
                          echo $data['total_number'];
                          ?>
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Requests Number Here</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Type</th>
                                <th>Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($all_data as $data)
                            {
                                ?>
                                <tr>
                                    <td><?=$data['number']?></td>
                                    <td><?=$data['amount']?></td>
                                    <td>

                                        <?php
                                        if($data['status'] == 1){
                                            echo '<p class="badge badge-primary">Pending</p>';
                                        }elseif ($data['status'] == 2){
                                            echo '<p class="badge badge-success">Success</p>';
                                        }else{
                                            echo '<p class="badge badge-warning">Failed</p>';
                                        }
                                        ?>
                                    </td>
                                    <td><?=($data['type']=='1')? 'Prepaid':'Postpaid' ?></td>
                                    <td><?=date('F j, Y, g:i a',strtotime($data['created_at']))?></td>

                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
    include_once 'includes/footer.php';
?>