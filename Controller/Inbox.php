<?php
namespace App;
use PDO;
use PDOException;
class Inbox extends Connection
{

    public function success($data)
    {
        try{
            $query = "INSERT INTO `success` (`pay_status`, `other_currency`, `epw_txnid`, `mer_txnid`, `store_id`, `store_amount`, `bank_txn`, `card_number`, `card_type`, `ip_address`, `epw_card_bank_name`, `epw_card_bank_country`, `epw_card_risklevel`) VALUES (:pay_status, :other_currency, :epw_txnid, :mer_txnid, :store_id, :store_amount, :bank_txn, :card_number, :card_type, :ip_address, :epw_card_bank_name, :epw_card_bank_country, :epw_card_risklevel)";
            $stm = $this->dbh->prepare($query);
            
            $stm->execute(array(
                ':pay_status' => $data['pay_status'],
                ':other_currency' => $data['other_currency'],
                ':epw_txnid' => $data['epw_txnid'],
                ':mer_txnid' => $data['mer_txnid'],
                ':store_id' => $data['store_id'],
                ':store_amount' => $data['store_amount'],
                ':bank_txn' => $data['bank_txn'],
                ':card_number' => $data['card_number'],
                ':card_type' => $data['card_type'],
                ':ip_address' => $data['ip_address'],
                ':epw_card_bank_name' => $data['epw_card_bank_name'],
                ':epw_card_bank_country' => $data['epw_card_bank_country'],
                ':epw_card_risklevel' => $data['epw_card_risklevel'],
                ));
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    
    public function fail($failData)
    {
        try{
            $query = "INSERT INTO `fails` (`card_number`) VALUES (:card_number)";
            $stm = $this->dbh->prepare($query);
            $stm->execute(array(':card_number' => $failData));
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function fail_status()
    {
        try{

            $email = $this->dbh->prepare('SELECT `id` FROM `email` ORDER BY id DESC LIMIT 1');
            $email->execute();
            $this->email_id = $email->fetch(PDO::FETCH_ASSOC);

            $email = $this->dbh->prepare("select recharge.id from `recharge` left join `email` on recharge.email_id = email.id where email_id = ".$this->email_id ['id']);
            $email->execute();
            $records = $email->fetchAll(PDO::FETCH_ASSOC);
            foreach ($records as $record){
                $query = "UPDATE `recharge` SET `status`='3' WHERE  `id`=".$record['id'];
                $stm = $this->dbh->prepare($query);
                $stm->execute();
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function success_status()
    {
        try{

            $email = $this->dbh->prepare('SELECT `id` FROM `email` ORDER BY id DESC LIMIT 1');
            $email->execute();
            $this->email_id = $email->fetch(PDO::FETCH_ASSOC);

            $email = $this->dbh->prepare("select recharge.id from `recharge` left join `email` on recharge.email_id = email.id where email_id = ".$this->email_id ['id']);
            $email->execute();
            $records = $email->fetchAll(PDO::FETCH_ASSOC);
            foreach ($records as $record){
                $query = "UPDATE `recharge` SET `status`='2' WHERE  `id`=".$record['id'];
                $stm = $this->dbh->prepare($query);
                $stm->execute();
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}