<?php
namespace App;
use PDO;
use PDOException;
class Operator extends Connection
{
    public function operator($op)
    {
        try {
            $stm = $this->dbh->prepare("SELECT * FROM `recharge` WHERE operator = '$op'");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e)
        {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function select($query)
    {
        try {
            $stm = $this->dbh->prepare($query);
            $stm->execute();
           return $stm->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e)
        {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}