<?php
namespace App;
use PDO;
use PDOException;
class Recharge extends Connection
{

    public $number = array();
    public $amount = array();
    public $operator = array();
    public $type = array();
    public $email = '';

    public function set($data = array())
    {
        if(array_key_exists('number', $data))
        {
            $numbers = $data['number'];
            if(is_array($numbers)){
                $numbers = array_unique($numbers);
                foreach ($numbers as $number)
                {
                    $number = preg_replace('/[^0-9]/', '', $number);
                    if(strlen($number) == '11')
                    {
                        $this->number[] = $number;
                        $this->operator[] = substr($number, 1,2);
                    }
                }
            }else {
                $number = preg_replace('/[^0-9]/', '', $numbers);
                if(strlen($number) == '11')
                {
                    $this->number[] = $number;
                    $this->operator[] = substr($number, 1,2);
                }
            }
        }

        if(array_key_exists('amount', $data))
        {
            $amounts = $data['amount'];
            if(is_array($amounts)){
                foreach ($amounts as $amount)
                {
                    $amount = preg_replace('/[^0-9]/', '', $amount);
                    $amount = filter_var($amount, FILTER_VALIDATE_INT);
                    if(strlen($amount) > '1' && strlen($amount) <= '4')
                    {
                        if($amount > 9 && $amount <=1000){
                            $this->amount[] = $amount;
                        }
                    }
                }
            } else {
                $amount = preg_replace('/[^0-9]/', '', $amounts);
                $amount = filter_var($amount, FILTER_VALIDATE_INT);

                if(strlen($amount) > '1' && strlen($amount) <= '4')
                {
                    if($amount > 9 && $amount <=1000){
                        $this->amount[] = $amount;
                    }
                }
            }
        }

        if(array_key_exists('type', $data)){
            $operators = $data['type'];
            if(is_array($operators)){
                foreach ($operators as $operator){
                    if($operator == 'Prepaid'){
                        $this->type[] = 1;
                    }elseif ($operator == 'Postpaid'){
                        $this->type[] = 2;
                    }

                }
            }else {
                if($operators == 'Prepaid'){
                    $this->type[] = 1;
                }elseif ($operators == 'Prepaid'){
                    $this->type[] = 2;
                }
            }


        }

        if(array_key_exists('email', $data)){
            $email = $data['email'];
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);
            $this->email = $email;
        }



        return $this;
    }

    public function store()
    {
        $transaction_id = substr(md5(time()),'0','10');
        $this->dbh->exec("INSERT INTO `email` (`email`, `transaction_id`) VALUES ('$this->email', '$transaction_id')");


        $email = $this->dbh->prepare('SELECT `id` FROM `email` ORDER BY id DESC LIMIT 1');
        $email->execute();
        $this->email_id = $email->fetch(PDO::FETCH_ASSOC);


        for($i = 0; $i<count($this->number); $i++) {
            $query = "INSERT INTO `recharge` (`type`, `email_id`, `number`, `amount`, `operator`, `status`) VALUES (:type, :email, :number, :amount, :operator, 1);";
            
           
            
            $stmt = $this->dbh->prepare($query);

            $stmt->bindParam(':type', $this->type[$i], PDO::PARAM_STR);
            $stmt->bindParam(':email', $this->email_id['id'], PDO::PARAM_STR);
            $stmt->bindParam(':number', $this->number[$i], PDO::PARAM_STR);
            $stmt->bindParam(':amount', $this->amount[$i], PDO::PARAM_STR);
            $stmt->bindParam(':operator', $this->operator[$i], PDO::PARAM_STR);

            $confirm = $stmt->execute();
        }

        if($confirm){
           $this->index();
        }

    }

    public function index()
    {
        $email = $this->dbh->prepare('SELECT `id` FROM `email` ORDER BY id DESC LIMIT 1');
        $email->execute();
        $this->email_id = $email->fetch(PDO::FETCH_ASSOC);

        $email = $this->dbh->prepare("select `number`, `amount`, `email`, `transaction_id` from `recharge` left join `email` on recharge.email_id = email.id where email_id = ".$this->email_id ['id']." ORDER By email.id DESC");
        $email->execute();
        $records = $email->fetchAll(PDO::FETCH_ASSOC);

        foreach ($records  as $record) {
            $email = $record['email'];
            $tran_id = $record['transaction_id'];
            $amount[] = $record['amount'];
        }
        $post = [
            'store_id' => 'amarbill',
            'signature_key' => 'dc0c2802bf04d2ab3336ec21491146a3',
            'cus_name'   => $email,
            'cus_email'   => $email,
            'cus_phone'   => 'N/A',
            'amount'   =>  array_sum($amount),
            'tran_id'   => $tran_id,
            'currency'   => 'BDT',
            'success_url'   => 'http://amarbill.com/success.php',
            'fail_url'   => 'http://amarbill.com/fail.php',
            'cancel_url'   => 'http://amarbill.com/fail.php',
            'desc'   => 'Amarbill - Online Recharge & Bill Payment',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://securepay.easypayway.com/payment/index.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($ch);
        var_export($response);
    }

    public function all()
    {
        try {
            $stm = $this->dbh->prepare("SELECT * FROM `recharge` ORDER by id DESC");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e)
        {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}
