<?php
include_once 'vendor/autoload.php';

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $obj = new App\Recharge();

    $data =  $obj->set($_POST);

    if(count($data->number) == count($data->amount)) // check number array and amount array elements are same
    {
        $obj->store();
    }else{
        header('location:index.php');
    }
}else{
   header('location:index.php');
}


