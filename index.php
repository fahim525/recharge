<!DOCTYPE html>
<html>
<head>
<link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
<link rel="manifest" href="icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <title>Amarbill - Online Recharge & Bill Payment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/skins/all.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img width="180" src="assets/images/logo.png" alt="logo">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javaScript:void(0)" data-toggle="modal" data-target="#contact">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">GrameenPhone, Robi, Banglalink, Airtel & Teletalk</div>
                <div class="panel-body">
                    <form action="process.php" method="post" id="formValidation">
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                    <label>
                                        <input type="radio" checked name="type[]" value="Prepaid"> Prepaid
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                    <label>
                                        <input type="radio" name="type[]" value="Postpaid"> Postpaid
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input id="number" type="text" maxlength="11" onkeyup="return getNumber(this)" name="number[]" class="form-control input-sm" autofocus placeholder="Mobile Number (Required)*">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputAmount">Amount</label>
                                    <div class="input-group">
                                        <input id="amount" type="text" onkeyup="return getAmount(this)"  name="amount[]" class="form-control equipCatValidation input-sm" placeholder="Amount (Required)*">
                                        <div class="input-group-addon">
                                            <a href="javaScript:void(0)" id="add" title="Add New Item">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <span id="inputError"></span>
                                <span id="sameNumber"></span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" required onkeyup="return getEmail(this)" name="email" class="form-control input-sm" placeholder="Email Address (Required)*">
                                </div>
                                <span id="emailInputError"></span>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-condensed">
                                    <tbody id="view"></tbody>
                                </table>
                            </div>
                        </div>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary btn-block"> <span class="fa fa-shopping-cart"></span> Submit</button>
                    </form>
                </div>
                <div class="panel-footer">
                    <p>By clicking Submit, I accept <a href="" data-toggle="modal" data-target="#myModal">Terms & Conditions</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span style="display: inline-block;padding-top: 8px">Amarbill © 2018</span>
                <img class="pull-right" src="assets/images/all.png" alt="">
            </div>
        </div>
    </div>
</footer>
<!--contact us-->

<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Contact Us</h4>
            </div>
            <div class="modal-body">
                <address>
                    <p><strong>Address:</strong></p>
                    <p>KA-32/6, Shahjadpur,</p>
                    <p>Pragoti Sharoni, Gulshan,</p>
                    <p>Dhaka 1212, Bangladesh</p>
                    <p><strong>Opening Times:</strong></p>
                    <p> 24X7 Customer Service</p>

                    <strong>Email:</strong> <a href="mailto:contact@amarbill.com">contact@amarbill.com</a>
                    <br /><br />
                    <strong>Mobile:</strong> <a href="tel:+8801712345678">+8801712345678</a>, <a href="tel:+8801711123123">+8801711123123</a>, <a href="tel:+8801716532191">+8801716532191</a>
                    <br /> <br />
                    <strong>Facebook:</strong> <a href="http://facebook.com/Shobbai">http://facebook.com/Shobbai</a>
                    <br /> <br />
                    <strong>Twitter</strong>: <a href="http://twitter.com/Shobbai">http://twitter.com/Shobbai</a>
                </address>
            </div>
        </div>
    </div>
</div>

<!-- Terms Condition -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>All sales are final & non-refundable.</li>
                    <li>Please wait up to 60 minutes to receive SMS confirmation for airtime recharge.</li>
                    <li>We will assume no liability if the recipient’s mobile number is entered incorrectly and no refunds will be issued.</li>
                    <li>If you have accidentally chosen the wrong connection type, please email us immediately by contacting (<a href="mailto:contact@amarbill.com">contact@amarbill.com</a>).</li>
                    <li>We will not reconcile any complaints made after 7 days of the actual transaction. Failure to do so within the maximum stipulate time will be considered successful.</li>
                </ul>
            </div>
        </div>
    </div>
</div>


<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/icheck.js"></script>
<script src="assets/js/custom.js"></script>
</body>
</html>