-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2018 at 03:41 PM
-- Server version: 10.0.31-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uttaraex_recharge`
--

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `transaction_id`) VALUES
(166, 'emran@shobbai.com', '593dcaf516'),
(167, 'shakil@fg.com', '3650d8fd2f'),
(168, 'emran@shobbai.com', '4c809e3a5d'),
(169, 'razutest@mail.com', 'f79d7399a2'),
(170, 'emran@shobbai.com', 'adeaf2d6e5'),
(171, 'emran@shobbai.com', 'ab30197379'),
(172, 'emran@shobbai.com', '7908690dd5'),
(173, 'emran@shobbai.com', '7cdcee38b7'),
(174, 'emran@shobbai.com', '33a9174efb'),
(175, 'riad@thecodero.com', 'e57a111aa0'),
(176, 'me@saaqeb.com', '0415ee53f1'),
(177, 'r3h4n.1337@gmail.com', 'a6396da2c4'),
(178, 'intiaz135246@gmail.com', '0adff0f6aa'),
(179, 'intiaz135246@gmail.com', 'ef5cec6144'),
(180, 'test@test.com', 'ed409b93ea'),
(181, 'musfiqurrahaman7@gmail.com', '36530021f0'),
(182, 'musfiqureahaman7@gmail.com', 'c810c9748b'),
(183, 'musfiqurrahaman7@gmail.com', '607c08495a'),
(184, 'payment@amarbill.com', '4264a41894'),
(185, 'payment@amarbill.com', '98d126ff0c'),
(186, 'Classicmasud18@gmail.com', '9b0caec616'),
(187, 'musfiqurrahaman7@gmail.com', '3ca434e370'),
(188, 'emran@shobbai.com', 'f89c9ad8ae'),
(189, 'Classicmasud18@gmail.com', 'bf305f3b54'),
(190, 'Classicmasud18@gmail.com', '947a09487a'),
(191, 'musfiqurrahaman7@gmail.com', '35a254b031'),
(192, 'mdp3772@gmail.com', '13a4545c62'),
(193, 'farhadkabirayub3201@gmail.com', 'e999a4851c'),
(194, 'farhadkabirayub3201@gmail.com', '37337d4e5f'),
(195, 'farhadkabirayub3201@gmail.com', '6a2b4e2b50'),
(196, 'farhadkabirayub3201@gmail.com', 'bf9f7ebd5a'),
(197, 'farhadkabirayub3201@gmail.com', '8a97f53f1b'),
(198, 'farhadkabirayub3201@gmail.com', '1750a86fca'),
(199, 'farhadkabirayub3201@gmail.com', '48705d9034'),
(200, 'farhadkabirayub3201@gmail.com', 'b1d3140172'),
(201, 'farhadkabirayub3201@gmail.com', 'cfa8a75b35'),
(202, 'farhadkabirayub3201@gmail.com', '2e6b246dfd');

-- --------------------------------------------------------

--
-- Table structure for table `fails`
--

CREATE TABLE `fails` (
  `id` int(11) NOT NULL,
  `card_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fails`
--

INSERT INTO `fails` (`id`, `card_number`) VALUES
(26, 'null'),
(27, 'null'),
(28, '539932******4303'),
(29, '999999*********7454'),
(30, '999999*********7454'),
(31, '999999*********7454');

-- --------------------------------------------------------

--
-- Table structure for table `recharge`
--

CREATE TABLE `recharge` (
  `id` int(11) NOT NULL,
  `email_id` int(255) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `number` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recharge`
--

INSERT INTO `recharge` (`id`, `email_id`, `type`, `number`, `amount`, `operator`, `created_at`, `status`) VALUES
(180, 175, 2, '01819231922', 10, '18', '2018-01-21 08:26:53', '1'),
(181, 176, 2, '01922999333', 100, '19', '2018-01-21 09:38:07', '1'),
(182, 177, 1, '01717014972', 1000, '17', '2018-01-21 11:22:12', '1'),
(183, 177, 1, '01710565531', 1000, '17', '2018-01-21 11:22:12', '1'),
(184, 177, 1, '01834269415', 1000, '18', '2018-01-21 11:22:12', '1'),
(185, 177, 1, '01950436697', 1000, '19', '2018-01-21 11:22:12', '1'),
(186, 178, 1, '01740135246', 10, '17', '2018-01-21 13:18:31', '3'),
(187, 179, 1, '01740135246', 10, '17', '2018-01-21 13:43:27', '2'),
(188, 180, 2, '01716532191', 10, '17', '2018-01-21 16:58:56', '1'),
(189, 181, 1, '01787202645', 31, '17', '2018-01-21 17:02:41', '3'),
(190, 182, 1, '01787202645', 31, '17', '2018-01-21 17:11:52', '3'),
(191, 183, 1, '01787202645', 31, '17', '2018-01-21 17:15:23', '1'),
(192, 184, 1, '01711123123', 10, '17', '2018-01-21 17:15:58', '2'),
(193, 185, 1, '01711123123', 10, '17', '2018-01-21 17:37:43', '1'),
(194, 186, 1, '01753096173', 30, '17', '2018-01-21 17:39:01', '2'),
(195, 187, 1, '01787202645', 31, '17', '2018-01-21 17:42:55', '3'),
(196, 188, 1, '01711123123', 10, '17', '2018-01-21 17:54:34', '1'),
(197, 189, 1, '01753096173', 100, '17', '2018-01-21 18:09:37', '1'),
(198, 190, 1, '01994730904', 100, '19', '2018-01-21 18:17:59', '1'),
(199, 191, 1, '01787202645', 31, '17', '2018-01-21 18:45:37', '2'),
(200, 192, 1, '01931439114', 20, '19', '2018-01-21 19:03:18', '1'),
(201, 193, 1, '01723112397', 18, '17', '2018-01-22 07:09:49', '1'),
(202, 194, 1, '01723112397', 18, '17', '2018-01-22 07:10:09', '1'),
(203, 195, 1, '01723112397', 18, '17', '2018-01-22 07:11:10', '1'),
(204, 196, 1, '01723112397', 18, '17', '2018-01-22 07:11:19', '1'),
(205, 197, 1, '01723112397', 18, '17', '2018-01-22 07:11:49', '1'),
(206, 198, 1, '01723112397', 18, '17', '2018-01-22 07:12:09', '1'),
(207, 199, 1, '01723112397', 18, '17', '2018-01-22 07:14:01', '1'),
(208, 200, 1, '01723112397', 18, '17', '2018-01-22 07:14:21', '1'),
(209, 201, 1, '01723112397', 18, '17', '2018-01-22 07:14:29', '1'),
(210, 202, 1, '01723112397', 18, '17', '2018-01-22 07:14:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `success`
--

CREATE TABLE `success` (
  `id` int(11) NOT NULL,
  `pay_status` varchar(255) NOT NULL,
  `other_currency` int(11) NOT NULL,
  `epw_txnid` varchar(255) NOT NULL,
  `mer_txnid` varchar(255) NOT NULL,
  `store_id` varchar(255) NOT NULL,
  `store_amount` varchar(255) NOT NULL,
  `bank_txn` varchar(255) NOT NULL,
  `card_number` varchar(255) NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `epw_card_bank_name` varchar(255) NOT NULL,
  `epw_card_bank_country` varchar(255) NOT NULL,
  `epw_card_risklevel` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `success`
--

INSERT INTO `success` (`id`, `pay_status`, `other_currency`, `epw_txnid`, `mer_txnid`, `store_id`, `store_amount`, `bank_txn`, `card_number`, `card_type`, `ip_address`, `epw_card_bank_name`, `epw_card_bank_country`, `epw_card_risklevel`, `created_at`) VALUES
(2, 'Successful', 10, 'AMA1516556267103078', '98d126ff0c', 'amarbill', '9.80', '5AL3Z3FXC5', '01682300022', 'bKash-bKash', '119.30.35.138', '', '', '', '2018-01-21 17:39:43'),
(3, 'Successful', 31, 'AMA1516560341103624', '35a254b031', 'amarbill', '30.37', '5AM9Z3K7IL', '01711086314', 'bKash-bKash', '103.205.69.25', '', '', '', '2018-01-21 18:49:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fails`
--
ALTER TABLE `fails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recharge`
--
ALTER TABLE `recharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id` (`email_id`);

--
-- Indexes for table `success`
--
ALTER TABLE `success`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `fails`
--
ALTER TABLE `fails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `recharge`
--
ALTER TABLE `recharge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `success`
--
ALTER TABLE `success`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `recharge`
--
ALTER TABLE `recharge`
  ADD CONSTRAINT `recharge_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `email` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
